# This file may be used to create an environment using:
# $ conda create --name <env> --file <this file>
# platform: win-64
ca-certificates=2020.1.1=0
certifi=2019.11.28=py27_0
numpy=1.16.6=pypi_0
opencv-contrib-python=4.2.0.32=pypi_0
pip=19.3.1=py27_0
python=2.7.18=hfb89ab9_0
setuptools=44.0.0=py27_0
sqlite=3.30.1=h0c8e037_0
vc=9=h7299396_1
vs2008_runtime=9.00.30729.1=hfaea7d5_1
wheel=0.33.6=py27_0
win-unicode-console=0.5=pypi_0
wincertstore=0.2=py27hf04cefb_0
