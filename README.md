# README.md

This Project is used for Robotic Class

Contributed By :
- @dennis.febri.dien
- @yogi.lesmana

Description :
This is repository for object detection using open cv.
This project using python 2 for compatibility reason with ROS Robotic Melodic.
This project used for sandbox algorithm exclusive for object detection. Will be integrated into this repo :
https://gitlab.com/dennis.febri.dien/ros-robotic/

Currently Task :
- [x] Implement Camera.py (To show camera with laptop camera)
- [ ] Implement Object Detection using Simple Detection (Color Threshold)
- [ ] Integrate Algorithm to ROS Melodic